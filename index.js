const ul = document.getElementById('contactlist')

const form = document.getElementById('form')
form.addEventListener('submit', handleformsubmission)
function handleformsubmission(event) {
    event.preventDefault()
const Name = form['Name'].value
const Phone = form['Phone'].value
const Image = form['Image'].value

if (!Name) {
    console.log("name cannot be null")
    return
}
else if (Name.trim() === ""){
      console.log("name cannot be just spaces")
       return
}
const contact = createContact(Name,Phone,Image)
ul.appendChild(contact)
}

function createContact(Name, Phone, Image) {

const li = document.createElement('li')
const img = document.createElement('img')
img.src = Image
li.appendChild(img)
const div = document.createElement('div')
li.appendChild(div)
const h3 = document.createElement('h3')
h3.innerHTML = Name
div.appendChild(h3)
const span = document.createElement('span')
span.innerHTML = Phone
div.appendChild(span)
ul.appendChild(li)
return li


}